#include "CmdLineArgParser.h"
#include <algorithm>

const std::string Application::CmdLineArgParser::noOption;

Application::CmdLineArgParser::CmdLineArgParser(int& argc, char *argv[])
    : args(new std::vector<std::string>)
{
    for (int i = 1; i < argc; ++i)
    {
        args->push_back(std::string(argv[i]));
    }
}

const std::string& Application::CmdLineArgParser::getOption(const std::string& option) const
{
    if (option == ""  && args->size() == 2)
    {
        return args->at(1);
    }

    std::vector<std::string>::iterator it = std::find(args->begin(), args->end(), option);

    // Retrieve option's value, which is next to the option itself
    // NOTE This is inherently flawed since it does not consider the case, where a value
    //       for argument is forgotten, which creates a situation, where the value of arg
    //       N is arg N+1. This is not a problem, since value-checking needs to happen inside
    //       the class, that inherits
    if (it != args->end() && ++it != args->end())
        return *it;

    return noOption;
}

bool Application::CmdLineArgParser::isOption(const std::string& option) const
{
    return std::find(args->begin(), args->end(), option)
           != args->end();
}

size_t Application::CmdLineArgParser::numOfArgs()
{
    return args->size();
}
