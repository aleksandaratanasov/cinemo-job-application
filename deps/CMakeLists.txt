cmake_minimum_required(VERSION 2.8)

project(deps)

# All dependencies are to be built as static
add_subdirectory(libmp3lame-CMAKE)

if (WITH_TESTS)
  add_subdirectory(googletest)
  set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH} " )
endif()

if (WITH_LOGGING)
  set(build_static_lib ON CACHE BOOL "" FORCE)
  #set(test ${WITH_TESTS} CACHE BOOL "" FORCE)
  add_subdirectory(easyloggingpp)
endif()

if (WITH_JSON)
  set(JSON_BuildTests OFF CACHE INTERNAL "")
  add_subdirectory(json)
endif()
