#ifndef CONIFURATION_H
#define CONIFURATION_H

#include <sstream>

namespace Application
{
const unsigned short BITRATE_DEFAULT = 128;
const unsigned char QUALITY_DEFAULT = 5;
const std::string INPUT_DIR_DEFAULT = ".";
const std::string OUTPUT_DIR_DEFAULT = ".";
const bool MULTIRHEADED_DEFAULT = true;
/**
  * @brief The ApplicationConfiguration is used as a storage for various settings
  *        regarding the application such as a boiled down subset of LAME encoder
  *        related settings.
  *
  *        Note: The number of channels (mono, stereo etc.) is not exposed since LAME automatically
  *        calculates it based on compression ratio and input channels
  */
typedef struct ApplicationConfiguration
{
    // LAME encoder settings (not all!)
    unsigned short bitRate;     //!< Bit rate - default 128
    unsigned char quality;      //!< Encoding quality (\see Quality)

    // Misc application settings
    bool multithreaded;         //!< Enable/Disable multithreaded encoding

    //!< Directory where the input audio files
    //!< will be retrieved from (default: current directory)
    //!< WARNING: On older versions of Windows there is a limit to the number of
    //!< character a path can have!
    std::string inputDir;

    //!< Directory where the input audio files
    //!< will be retrieved from (default: current directory)
    //!< WARNING: On older versions of Windows there is a limit to the number of
    //!< character a path can have!
    std::string outputDir;

    ApplicationConfiguration()
        : bitRate(BITRATE_DEFAULT),
          quality(QUALITY_DEFAULT),
          multithreaded(MULTIRHEADED_DEFAULT),
          inputDir(INPUT_DIR_DEFAULT),
          outputDir(OUTPUT_DIR_DEFAULT)
    {
    }

    std::string prettyPrint()
    {
        std::stringstream contents;
        contents << "Configuration" <<
                 "\n\tBitrate: " << std::to_string(bitRate) <<
                 "\n\tQuality: " << std::to_string(quality) <<
                 "\n\tMultithreading: " << (multithreaded ? "on" : "off") <<
                 "\n\tInput directory: " << inputDir <<
                 "\n\tOutput directory: " << outputDir;

        return contents.str();
    }
} Configuration;
}

#endif // CONIFURATION_H
