#include <gtest/gtest.h>
#include <Queue.h>
#include <thread>

TEST(testOperations, EnqueueDequeue)
{
    Application::Threads::Queue<int> q;
    for (unsigned int i = 0; i < 100; i++)
    {
        q.enqueue(i);
    }
    int v = 0;
    for (unsigned int i = 0; i < 100; i++)
    {
        v = q.dequeue();
        EXPECT_EQ(v, i);
    }
}

TEST(testOperations, EnqueueDequeueOverload)
{
    Application::Threads::Queue<int> q;
    for (unsigned int i = 0; i < 100; i++)
    {
        q << i;
    }
    int v = 0;
    for (unsigned int i = 0; i < 100; i++)
    {
        q >> v;
        EXPECT_EQ(v, i);
    }
}

// TODO Check race condition sanitizers for testing concurrency

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
