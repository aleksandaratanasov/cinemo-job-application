cmake_minimum_required(VERSION 2.8)

project(tests)

include_directories(${CMAKE_SOURCE_DIR})
add_executable(test_queue test_queue.cpp)
target_link_libraries(test_queue gtest)
