#ifndef QUEUE_H
#define QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

namespace Application
{
namespace Threads
{

// https://stackoverflow.com/questions/15278343/c11-thread-safe-queue
template <class T>
class Queue
{
    public:
        Queue()
            : queue(),
              mutex(),
              condition()
        {
        }

        /**
         * @brief Add element to the back of the queue and notifies
         *        that an item is available for dequeueing
         * @return Element at the front of the queue
         */
        void enqueue(T t)
        {
            // https://stackoverflow.com/questions/20516773/stdunique-lockstdmutex-or-stdlock-guardstdmutex
            std::lock_guard<std::mutex> lock(mutex);
            queue.push(t);
            condition.notify_one();
        }

        /**
         * @brief Convenience overloading for enqueueing items
         * @param Item to enqueue to the back of the queue
         * @return
         */
        Queue& operator<<(T t)
        {
            enqueue(t);
        }

        /**
         * @brief Retrieves front element from the queue. In case it is
         *        the queue is empty, wait till a element is avaiable.
         * @return Element at the front of the queue
         */
        T dequeue(void)
        {
            // https://stackoverflow.com/questions/20516773/stdunique-lockstdmutex-or-stdlock-guardstdmutex
            std::unique_lock<std::mutex> lock(mutex);
            while (queue.empty())
            {
                // release lock as long as the wait and reaquire it afterwards.
                condition.wait(lock);
            }
            T val = queue.front();
            queue.pop();
            return val;
        }

        /**
         * @brief Convenience overloading for dequeueing items
         * @param Item extracted from the front of the queue
         * @return
         */
        Queue& operator>>(T& t)
        {
            t = dequeue();
        }

    private:
        std::queue<T> queue;
        mutable std::mutex mutex;
        std::condition_variable condition;
};

}
}

#endif //QUEUE_H
