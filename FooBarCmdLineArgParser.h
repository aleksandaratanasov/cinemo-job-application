#ifndef FOOBARCMDLINEARGPARSER_H
#define FOOBARCMDLINEARGPARSER_H

#ifdef WITH_JSON
#include <json.hpp>
#endif

#include <CmdLineArgParser.h>
#include <Configuration.h>

namespace Application
{
class FooBarCmdLineArgParser : protected CmdLineArgParser
{
    public:
        FooBarCmdLineArgParser(int &argc, char *argv[]);

        std::shared_ptr<Configuration> getConfig();
        std::string getHelp();

    private:
        //!< The help how to use the application. Includes examples
        static const std::string help;
        //!< The configuration of all configurable components of the application
        static std::shared_ptr<Configuration> config;

#ifdef WITH_JSON
        //!< Contains the loaded contents of the JSON configuration file
        nlohmann::json jObj;

        /**
         * @brief Parses the JSON configuration file
         * @return True if parsing completed without any errors
         */
        bool parseJson();
        /**
         * @brief Parses the bitrate from the JSON configuration file
         * @return True if the bitrate was parsed without any errors
         *         and within range
         */
        bool parseBitRate();
        /**
         * @brief Parses the quality level from the JSON configuration file
         * @return True if the quality level was parsedwithout any errors
         *         and within range
         */
        bool parseQuality();

        /**
         * @brief Parses the setting, which determines whether multithreading
         *        will be used or not
         * @return True if parsing fails or is successful and set to true
         */
        bool parseMultiThreaded();
#endif

        /**
         * @brief Parses and validates the input directory
         * @return True if no error has occurred. Internally the configuration
         *         input directory is set to whatever value was retrieved. Refer
         *         to \see checkDir() for more information on validation of the
         *         directory
         */
        bool parseInputDir();
        /**
         * @brief Parses and validates the output directory
         * @return True if no error has occurred. Internally the configuration
         *         output directory is set to whatever value was retrieved. Refer
         *         to \see checkDir() for more information on validation of the
         *         directory
         */
        bool parseOutputDir();

        /**
         * @brief Checks if directory is valid.
         * @param dirPath Path to a valid directory
         * @param checkEmpty Checks if directory is empty. If \see extensions
         *                   not empty, \see dir will be checked for containing
         *                   files with given extensions
         * @param extensions A list of extensions. Used only if \see checkEmpty
         *                   set to true. At least one file of each extension
         *                   needs to be found for \see dir to be considered not
         *                   empty
         * @return True if \see dir points to a valid directory (including empty
         *         check if enabled)
         */
        bool checkDir(const std::string& dirPath, bool checkEmpty = false, const std::vector<std::string>& extensions = std::vector<std::string>());

        /**
         * @brief In-place conversion of a string to lower case. Limitations set
         *        by the use of \see std::to_lower(). Used for handling the extensions
         *        of files
         * @param A string that will be converted to lower case
         */
        void strToLower(std::string& str);

        /**
         * @brief Extracts the extension from a path. Complex extensions (containing
         *        multiple dots) and check if path is actually a file are not done.
         * @param filePath
         * @return
         */
        std::string getExtension(const std::string& filePath);

        /**
         * @brief Generates the contents of \see help
         * @return A string with the help information
         */
        static std::string generateHelp();
};
}

#endif // FOOBARCMDLINEARGPARSER_H
