# Cinemo Job Application

Coding task for the application process at Cinemo (Karlsruhe)

## Description

Multithreaded encoding using LAME MP3 to convert one or multiple WAVE RIFF to MP3 audio files. The software requires `cmake` version 2.8 (or higher) as well as a compiler supporting C++14 (due to threading and filesystem cross-platform functionality)

## Licensing

The software is licensed under the term of GPLv2. For more information see LICENSE.txt

The following thrid party software is included. For more information on licensing see the 
respective license/copyright file in the respective repository

## Configuration with `cmake`

The project offers several features some of which are optional

| Dependency             | `cmake` option      | Description                     |
| :--------------------- |:------------------- | :------------------------------ |
| **LAME MP3 encoder**   | *mandatory*         | Encoding functionality WAV, MP3 |
| **easylogging++**      | `-DWITH_LOGGING=ON` | Logging functionality           |
| **SimpleJSON**         | `-DWITH_JSON=ON`    | Configuration via JSON file     |
| **GoogleTest**         | `-DWITH_TEST=ON`    | Build test                      |
| **Git**                | `-DWITH_GIT=ON`     | Enables automatic retrieval and update of dependencies. Requires connection to repositories. Check `.gitmodules` for details |
| **Doxygen**            | `-DWITH_DOCS=ON` and `-DCMAKE_BUILD_TYPE=Release` | Builds code documentation if doxygen is installed using `docs/Doxyfile.in` configuration. Available only for `Release` builds |

