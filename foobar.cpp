#include <iostream>
#include <utility>

#ifdef WITH_LOGGING
#include <easylogging++.h>
INITIALIZE_EASYLOGGINGPP
#endif

#include <CmdLineArgParser.h>

int main(int argc, char* argv[])
{
    std::cout << "foobar (multi-threaded encoder based on LAME\n"
              << "(c) Aleksandar Atanasov 2021\n" << std::endl;
}
