#ifndef CMDLINEARGPARSER_H
#define CMDLINEARGPARSER_H

#include <memory>
#include <vector>

namespace Application
{
/**
 * @brief The CmdLineArgParser class parses the command line arguments. It includes
 *
 *  * check if argument exists
 *  * value retrieval (as string) for an existing argument
 *
 * Further processing of a retrieved argument's value can be done accordingly by
 * inheriting from this class and adding the missing functionality
 *
 * Source: https://stackoverflow.com/a/868894/1559401
 */
class CmdLineArgParser
{
    protected:
        CmdLineArgParser(int &argc, char *argv[]);

        /**
         * @brief Retrieves a command line argument
         * @param option The argument's name that will be retrieved
         * @return Value of argument as string, empty if argument unknown
         */
        const std::string& getOption(const std::string& option) const;

        /**
         * @brief Checks if a given name represents a valid command line argument
         * @param option The argument's name that will be checked
         * @return True if argument exists
         */
        bool isOption(const std::string& option) const;

        size_t numOfArgs();

        static const std::string noOption;

    private:
        //!< A convenience container for all the arguments and their values
        std::unique_ptr<std::vector<std::string> > args;
};
}
#endif // CMDLINEARGPARSER_H
