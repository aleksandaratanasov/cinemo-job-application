#include "FooBarCmdLineArgParser.h"

// FIXME __has_include is a C++17 feature. Find workaround
/*
#if __has_include(<filesystem>)
#include <filesystem>
namespace fs = std::filesystem;
#elif __has_include(<experimental/filesystem>)
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#else
#error "Missing the <filesystem> header."
#endif
*/
// Not exactly a solution to the failing __has_include but
// the best I can come up with for now
#if __cplusplus == 201402L
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#elif __cplusplus >= 201703L
#include <filesystem>
namespace fs = std::experimental::filesystem;
#else
#error "Selected C++ or compiler not supporting filesystem library"
#endif

#include <map>
#include <algorithm>
#include <iostream>

#ifdef WITH_LOGGING
#include <easylogging++.h>
#endif

#ifdef WITH_JSON
#include <json.hpp>
#include <fstream>
#include <streambuf>

using json = nlohmann::json;
#endif

const std::string Application::FooBarCmdLineArgParser::help = Application::FooBarCmdLineArgParser::generateHelp();

std::shared_ptr<Application::Configuration> Application::FooBarCmdLineArgParser::config(new Application::Configuration());

Application::FooBarCmdLineArgParser::FooBarCmdLineArgParser(int& argc, char* argv[])
    : CmdLineArgParser(argc, argv)
{
}

std::shared_ptr<Application::Configuration> Application::FooBarCmdLineArgParser::getConfig()
{
    // When no argument or a single argument for help is used
    if (numOfArgs() == 0
            || ((numOfArgs() == 1)
                && (isOption("-h") || isOption("--help") || isOption("-?"))))
    {
        return std::shared_ptr<Configuration>(nullptr);
    }

    // When a single argument (that is not help) is used we assume that it's the input/output directory
    if (numOfArgs() == 1)
    {
        // Fail if not possible to retrieve input directory
        bool inputDirOk = parseInputDir();
        if (!inputDirOk)
        {
            return std::shared_ptr<Configuration>(nullptr);
        }

        // Set output directory to be the same as input
        config->outputDir = config->inputDir;

        return config;
    }

#ifdef WITH_JSON
    // If configuration is passed, we need to process the JSON file
    if (isOption("-c") || isOption("--configuration"))
    {
        // Settings like the input and output directory will be
        // replaced with the values from the -i and -o command
        // line arguments, if these are available
        bool jsonOk = parseJson();

        if (!jsonOk)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "Parsing JSON configuration file failed";
#endif
            return std::shared_ptr<Configuration>(nullptr);
        }
    }
    else
    {
        // Otherwise we expect the -i and -o arguments
#ifdef WITH_LOGGING
        LOG(WARNING) << "Missing configuration argument '-c'";
#endif
        bool inputDirOk = parseInputDir();
        bool outputDirOk = parseOutputDir();

        // Failing to retrieve the input directory leads to full failure
        if (!inputDirOk)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "Unable to retrieve input directory";
#endif
            return std::shared_ptr<Configuration>(nullptr);
        }

        // If the output directory  was not retrieved properly, we fallback
        // to the default behaviour from a single argument (not help) case
        // where input is also output directory
        if (!outputDirOk)
        {
#ifdef WITH_LOGGING
            LOG(WARNING) << "Unable to retrieve output directory. Fallback "
                         "to using input as output directory";
#endif
            config->outputDir = config->inputDir;
        }
    }
#else
    bool inputDirOk = parseInputDir();
    bool outputDirOk = parseOutputDir();

    if (!(inputDirOk && outputDirOk))
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "Unable to retrieve input/output directories";
#endif
        return std::shared_ptr<Configuration>(nullptr);
    }
#endif

    return config;
}

std::string Application::FooBarCmdLineArgParser::getHelp()
{
    return help;
}

#ifdef WITH_JSON
bool Application::FooBarCmdLineArgParser::parseJson()
{
    std::string jsonPath = (isOption("-c") ? getOption("-c") : getOption("--configuration"));
    if (jsonPath == "")
    {

#ifdef WITH_LOGGING
        LOG(ERROR) << "Unable to retrieve value for argument '-c/--configuration'";
#endif
        return false;
    }

    // Try to open JSON file
    std::fstream jF(jsonPath);

    // Set exceptions to be thrown on failure (https://stackoverflow.com/a/36613975/1559401)
    jF.exceptions(std::fstream::failbit | std::fstream::badbit);

    // Check if file can be opened
    if (!jF.is_open())
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "Error while trying to open JSON configuration file";
#endif
    }

    // Reserve space for file's content ahead - faster then using std::string automatic
    // memory allocation (https://stackoverflow.com/a/2602258/1559401)
    try
    {
        jF.seekg(0, std::ios::end);
        size_t size = jF.tellg();
        std::string buffer(size, ' ');
        jF.seekg(0);
        jF.read(&buffer[0], size);
        jF.close();

        // Generate JSON object from configuration file's contents
        try
        {
            jObj = json::parse(buffer);
        }
        catch (json::parse_error& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "Parsing JSON configuration has failed\n" << jE.what();
#endif
            return false;
        }

        // Populate configuration with extracted settings
        bool bitrateOk = parseBitRate();
        bool qualityOk = parseQuality();
        bool multithreadedOk = parseMultiThreaded();
        bool inputDirOk = parseInputDir();
        bool outputDirOk = parseOutputDir();

        if (!(bitrateOk && qualityOk
                && multithreadedOk
                && inputDirOk && outputDirOk))
        {
            return false;
        }
    }
    catch (std::system_error& e)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "Error while trying to read JSON configuration file: "
                   << e.code().message();
#endif
        return false;
    }

    return true;
}

bool Application::FooBarCmdLineArgParser::parseBitRate()
{
    bool error = false;

    try
    {
        unsigned short bitRate = jObj.at("encoding").at("bitrate");
        // Check if bitrate is within range mentioned in lame.h
        if (bitRate < 8 || bitRate > 320)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "'bitrate' out of range 8-320";
#endif
            error = true;
        }

        config->bitRate = bitRate;
    }
    catch (json::out_of_range& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "Unable to find 'bitrate' inside 'encoding' configuration\n" << jE.what();;
#endif
        error = true;
    }
    catch (json::type_error& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "'bitrate' has invalid type. Expected number in range 8-320\n" << jE.what();
#endif
        error = true;
    }
    catch (json::other_error& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "An error occurred while processing 'bitrate' setting\n" << jE.what();
#endif
        error = true;
    }

    return !error;
}

bool Application::FooBarCmdLineArgParser::parseQuality()
{
    bool error = false;

    try
    {
        unsigned char quality = jObj.at("encoding").at("quality");
        // Check if bitrate is within range mentioned in lame.h
        if (quality < 1 || quality > 10)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "'quality' out of range 1-10";
#endif
            error = true;
        }

        config->quality = quality;
    }
    catch (json::out_of_range& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "Unable to find 'quality' inside 'encoding' configuration\n" << jE.what();
#endif
        error = true;
    }
    catch (json::type_error& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "'quality' has invalid type. Expected number in range 1-10\n" << jE.what();
#endif
        error = true;
    }
    catch (json::other_error& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "An error occurred while processing 'quality' setting\n" << jE.what();
#endif
        error = true;
    }

    return !error;
}

bool Application::FooBarCmdLineArgParser::parseMultiThreaded()
{
    bool error = false;

    try
    {
        bool multithreaded = jObj.at("application").at("multithreaded");
        config->multithreaded = multithreaded;
    }
    catch (json::out_of_range& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "Unable to find 'multithreaded' inside 'application' configuration\n" << jE.what();
#endif
        error = true;
    }
    catch (json::type_error& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "'multithreaded' has invalid type. Expected true/false\n" << jE.what();
#endif
        error = true;
    }
    catch (json::other_error& jE)
    {
#ifdef WITH_LOGGING
        LOG(ERROR) << "An error occurred while processing 'multithreaded' setting\n" << jE.what();
#endif
        error = true;
    }

    return !error;
}
#endif

bool Application::FooBarCmdLineArgParser::parseInputDir()
{
    bool error = false;
    std::string dir = "";

    // Retrieve path
    // If only a single argument is given take it as the path
    if (numOfArgs() == 1)
    {
        dir = getOption("");
    }
    // If the input directory argument is set, take it. This will also prevent
    // the configuration file (if enabled) to overwrite the setting
    else if (isOption("-i") || isOption("--input"))
    {
        dir = isOption("-i") ? getOption("-i") : getOption("--input");
    }
#ifdef WITH_JSON
    // If no input directory argument is set, path will be extracted from the
    // configuration file
    else if (isOption("-c") || isOption("--configuration"))
    {
        try
        {
            dir = jObj.at("application").at("inputDir");
        }
        catch (json::out_of_range& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "Unable to find 'inputDir' inside 'application' configuration\n" << jE.what();;
#endif
            error = true;
        }
        catch (json::type_error& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "'inputDir' has invalid type. Expected path to valid directory\n" << jE.what();
#endif
            error = true;
        }
        catch (json::other_error& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "An error occurred while processing 'inputDir' setting\n" << jE.what();
#endif
            error = true;
        }
    }
#endif

    if (error)
    {
        return !error;
    }

    // Check if directory is valid
    std::vector<std::string> exts = { "wav" };
    error = !checkDir(dir, true, exts);

    if (!error)
    {
        config->inputDir = dir;
    }

    return !error;
}

bool Application::FooBarCmdLineArgParser::parseOutputDir()
{
    bool error = false;
    std::string dir = "";

    // Retrieve path
    // If only a single argument is given take it as the path
    if (numOfArgs() == 1)
    {
        dir = getOption("");
    }
    // If the output directory argument is set, take it. This will also prevent
    // the configuration file (if enabled) to overwrite the setting
    else if (isOption("-o") || isOption("--output"))
    {
        dir = isOption("-o") ? getOption("-o") : getOption("--output");
    }
#ifdef WITH_JSON
    // If no output directory argument is set, path will be extracted from the
    // configuration file
    else if (isOption("-c") || isOption("--configuration"))
    {
        try
        {
            dir = jObj.at("application").at("outputDir");
        }
        catch (json::out_of_range& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "Unable to find 'outputDir' inside 'application' configuration\n" << jE.what();;
#endif
            error = true;
        }
        catch (json::type_error& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "'outputDir' has invalid type. Expected path to valid directory\n" << jE.what();
#endif
            error = true;
        }
        catch (json::other_error& jE)
        {
#ifdef WITH_LOGGING
            LOG(ERROR) << "An error occurred while processing 'outputDir' setting\n" << jE.what();
#endif
            error = true;
        }
    }
#endif

    if (error)
    {
        return !error;
    }

    // Check if directory is valid
    error = !checkDir(dir);

    if (!error)
    {
        config->outputDir = dir;
    }

    return !error;
}

bool Application::FooBarCmdLineArgParser::checkDir(const std::string& dirPath, bool checkEmpty, const std::vector<std::string>& extensions)
{
    bool error = false;

    fs::path dir(dirPath);
    bool exists = fs::exists(dir);
    bool isDir = fs::is_directory(dir);
    error = !exists && !isDir;

    if (!error && checkEmpty)
    {
        //Check if directory empty
        bool empty = true;
        // TODO Check permissions Windows and POSIX
        for (const auto & entry : fs::directory_iterator(dir))
        {
            try
            {
                if (fs::is_regular_file(entry))
                {
                    empty = false;
                    break;
                }
            }
            catch (fs::filesystem_error& fE)
            {
#ifdef WITH_LOGGING
                LOG(ERROR) << "Error occurred while reading file properties\n" << fE.what();
#endif
                return false;
            }
        }

        // Check if at least one file of each extension contained inside extensions can be found
        if (!empty && (extensions.size() > 0))
        {
            // Create a map, where the key is an extension and the value is
            // a flag, that is set to true once a file with the given key/extension
            // is found in the list of files
            std::map<std::string, bool> extMap;
            for (auto const& ext : extensions)
            {
                extMap[ext] = false;
            }


            for (const auto & entry : fs::directory_iterator(dir))
            {
                try
                {
                    // Skip directories and everything that is not a regular file
                    if (!fs::is_regular_file(entry))
                    {
                        continue;
                    }
                }
                catch (fs::filesystem_error& fE)
                {
#ifdef WITH_LOGGING
                    LOG(ERROR) << "Error occurred while reading file properties\n" << fE.what();
#endif
                    return false;
                }

                // Extract extension from file
                auto ext = getExtension(entry.path());

                // Check if extension inside map with extensions that need to be looked for
                if (extMap.find(ext) != extMap.end())
                {
                    extMap[ext] = true;
                }
            }

            std::vector<bool> extFoundResults;
            for (auto const& extFound : extMap)
            {
                extFoundResults.push_back(extFound.second);
            }

            // Check if any of the extensions was not found
            bool extSearchIncomplete = std::any_of(
                                           extFoundResults.begin(),
                                           extFoundResults.end(),
                                           [](bool x)
            {
                return !x;
            });

            error = extSearchIncomplete;
            if (error)
            {
#ifdef WITH_LOGGING
                LOG(ERROR) << "Unable to find all files with requested extensions";
#endif
            }
        }
    }

    return !error;
}

void Application::FooBarCmdLineArgParser::strToLower(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(),
                   [](unsigned char c)
    {
        return std::tolower(c);
    });
}

std::string Application::FooBarCmdLineArgParser::getExtension(const std::string& filePath)
{
    // Note that this doesn't handle weird extensions with multiple dots such as
    // my_file.ext1.ext2.ext3, where only the last substring after the last dot will
    // be taken as the actual extension
    size_t i = filePath.rfind('.', filePath.length());
    std::string ext = "";
    ext = filePath.substr(filePath.find_last_of(".") + 1);

    strToLower(ext);

    return ext;
}

std::string Application::FooBarCmdLineArgParser::generateHelp()
{
    std::stringstream helpContents;
    helpContents << "Supported arguments:\n"
                 << "<PATH>\n\tDirectory containing the WAVE RIFF files and where the MP3 will be written to\n"
                 << "\tSingle argument, no other arguments\n"
                 << "\tExample:\n"
                 << "\t\t(Windows) foobar.exe \"D:\\wavs\"\n"
                 << "\t\t(Linux) ./foobar \"/home/user/wavs\"\n\n"
                 << "-?, -h, --help\n\tDisplays this help message\n\n"
                 << "\tSingle argument, no other arguments\n"
                 << "\tExample:\n"
                 << "\t\t(Windows) foobar.exe -?\n"
                 << "\t\t          foobar.exe -h\n"
                 << "\t\t          foobar.exe --help\n"
                 << "\t\t(Linux) ./foobar -?\n"
                 << "\t\t        ./foobar -h\n"
                 << "\t\t        ./foobar --help\n\n"
                 << "-i, --input <PATH>\n"
                 << "\tDirectory containing the WAVE RIFF files\n"
                 << "\tExample:\n"
                 << "\t\t(Windows) foobar.exe -i \"D:\\wavs\"\n"
                 << "\t\t(Linux) ./foobar -i \"/home/user/wavs\"\n\n"
                 << "-o, --output <PATH>\n"
                 << "\tDirectory where the encoded MP3 files will be written to\n"
                 << "\tExample:\n"
                 << "\t\t(Windows) foobar.exe -o \"D:\\wavs\"\n"
                 << "\t\t(Linux) ./foobar -o \"/home/user/wavs\"\n\n"
#ifdef WITH_JSON
                 << "WARNING: -i/-o settings override the same settings set by configuration file (see '-c' argument)\n"
#endif
#ifdef WITH_JSON
                 << "\n-c, --configuration <PATH_TO_JSON_CONFIG>\n"
                 << "\tLoad configuration for LAME encoder from JSON file. If\n"
                 << "\tnot enabled defaults will be used (see end of help)\n"
                 << "\tExample:\n"
                 << "\t\t(Windows) foobar.exe -c \"D:\\config.json\"\n"
                 << "\t\t(Linux) ./foobar -c \"/home/user/config.json\"\n"
                 << "\tExample JSON file:\n"
                 << "\t\t{\n"
                 << "\t\t  \"encoding\":\n"
                 << "\t\t  {\n"
                 << "\t\t    \"bitrate\": 64,\n"
                 << "\t\t    \"quality\": 7\n"
                 << "\t\t  },\n"
                 << "\t\t  \"application\":\n"
                 << "\t\t  {\n"
                 << "\t\t    \"multithreaded\": true,\n"
                 << "\t\t    \"inputDir\": \"./audio_wav\",\n"
                 << "\t\t    \"outputDir\": \"./audio_mp3\"\n"
                 << "\t\t  }\n"
                 << "\t\t}\n\n"
#endif
#ifdef WITH_LOGGING
                 << "\n-l, --logToFile Toggles writing logs to file on and off\n\n"
#endif
                 << "Default settings:\n"
                 << "\tBitrate:" << std::to_string(Application::BITRATE_DEFAULT) << "\n"
                 << "\tQuality:" << std::to_string(Application::QUALITY_DEFAULT) << "\n"
                 << "\tMultithreaded:" << (Application::MULTIRHEADED_DEFAULT ? "on" : "off") << "\n";
    return helpContents.str();
}

